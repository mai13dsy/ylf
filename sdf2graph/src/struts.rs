use std::fmt::Debug;
use std::num::NonZeroU8;

type Vert<C> = [Option<C>; 8];

/// a molecule graph, stores in
/// |V|+2|E|
///
/// assumes <8 connections/molecule
/// assumes <256 atoms in total
#[derive(Debug)]
pub struct MolGraph<NLabel, ELabel>
where
    ELabel: Copy + Clone,
{
    n: Vec<(NLabel, Vert<(NonZeroU8, ELabel)>)>,
}

impl<NLabel, ELabel> MolGraph<NLabel, ELabel>
where
    ELabel: Copy + Clone,
{
    /// pushes a new node, returns its index
    pub fn push_node(&mut self, l: NLabel) -> NonZeroU8 {
        self.n.push((l, [None; 8]));
        NonZeroU8::new((self.n.len() - 1) as u8).unwrap()
    }

    /// pushes a new edge
    /// push_edge(from, to, label)
    pub fn push_edge(&mut self, from: NonZeroU8, to: NonZeroU8, l: ELabel) {
        debug_assert!((from.get() as usize) < self.n.len());
        debug_assert!((to.get() as usize) < self.n.len());

        let element = &mut self.n[from.get() as usize];
        push(&mut element.1, (to, l)).unwrap();
    }

    /// pushes a symmetric edge
    /// (internally pushes 2 edges)
    /// push_edge(a, b, label)
    pub fn push_sym_edge(&mut self, a: NonZeroU8, b: NonZeroU8, l: ELabel) {
        self.push_edge(a, b, l);
        self.push_edge(b, a, l);
    }

    pub fn neighbours(&self, index: NonZeroU8) -> Option<impl Iterator<Item = &'_ (NonZeroU8, ELabel)>> {
        self.get(index).map(|n| eiter(&n.1))
    }

    pub fn get_label(&self, index: NonZeroU8) -> Option<&NLabel> {
        self.get(index).map(|n| &n.0)
    }

    pub fn get(&self, index: NonZeroU8) -> Option<&(NLabel, Vert<(NonZeroU8, ELabel)>)> {
        self.n.get(index.get() as usize)
    }

    /// swap-removes a node, this is a rather expensive operation, as it needs to re-write all
    /// edges leading to the node
    pub fn remove(&mut self, index: NonZeroU8) -> (NLabel, [Option<(NonZeroU8, ELabel)>; 8])
    where
        ELabel: Ord,
    {
        let last = NonZeroU8::new((self.n.len() - 1) as u8).unwrap();
        let removed = self.n.swap_remove(index.get() as usize);

        // remove dangling connections to removed node
        for n in self.n.iter_mut() {
            filter(&mut n.1, |e| e.0 == index);
            defrag(&mut n.1);
        }

        // move connections to the swapped-in node
        for n in self.n.iter_mut() {
            eiter_mut(&mut n.1).map(|e| {
                if e.0 == last {
                    e.0 = index;
                }
            }).for_each(|()|{});
        }

        removed
    }

    pub fn filter<F: Fn(&NLabel) -> bool>(&mut self, pred: F)
    where
        ELabel: Ord,
    {
        let mut nodes = self.n.iter().enumerate();
        // skip dummy
        let _ = nodes.next();

        let indices: Vec<_> = nodes
            .filter(|(_i, e)| pred(&e.0))
            .map(|(i, _e)| NonZeroU8::new(i as u8).unwrap())
            .collect();

        for i in indices.into_iter().rev() {
            self.remove(i);
        }
    }

    //pub fn nodes(&self) -> std::slice::Iter<(NLabel, [Option<(NonZeroU8, ELabel)>; 8])> {
    pub fn nodes(
        &self,
    ) -> impl Iterator<Item = (NonZeroU8, &(NLabel, [Option<(NonZeroU8, ELabel)>; 8]))> {
        let mut iter = self.n.iter().enumerate();
        let _dummy = iter.next();
        iter.map(|(i, r)| (NonZeroU8::new(i as u8).unwrap(), r))
    }
    pub fn dot<W: std::io::Write>(&self, mut w: W)
    where
        ELabel: Debug,
        NLabel: Debug,
    {
        writeln!(w, "digraph {{").unwrap();
        for (idx, (label, inner)) in self.nodes() {
            writeln!(w, "{} [label={:?}]", idx, label).unwrap();
            for (tgt, label) in inner.iter().flatten() {
                writeln!(w, "{} -> {}  [label=\"{:?}\"]", idx, tgt, label).unwrap();
            }
        }
        writeln!(w, "}}").unwrap();
    }

    pub fn new_with_label(label: NLabel) -> Self {
        MolGraph {
            n: vec![(label, [None; 8])],
        }
    }

    pub fn sizes(&self) -> (usize, usize) {
        let nodes = self.nodes().count();
        let edges = self.nodes().flat_map(|n| eiter(&n.1 .1)).count();

        (nodes, edges)
    }
}

impl<NLabel, ELabel> Default for MolGraph<NLabel, ELabel>
where
    ELabel: Copy + Clone,
    NLabel: Default,
{
    // the first element is always invalid to be able to use non-zero u8 without penality
    fn default() -> Self {
        MolGraph {
            n: vec![(NLabel::default(), [None; 8])],
        }
    }
}

fn push<E>(vec: &mut Vert<E>, element: E) -> Result<(), ()> {
    for e in vec.iter_mut() {
        if e.is_some() {
            continue;
        } else {
            *e = Some(element);
            return Ok(());
        }
    }
    Err(())
}

/// replaces things matching the predicate with none.
/// this leaves gaps.
fn filter<'a, E, I, F:>(i: I, pred: F)
where F: Fn(&'_ E) -> bool,
      I: IntoIterator<Item= &'a mut Option<E>>,
      E: 'a
{
    for e in i.into_iter() {
        if let Some(i) = e {
            if pred(&i) {
                *e = None
            }
        }
    }
}

/// moves all elements to the front.
fn defrag<E: Ord>(vec: &'_ mut Vert<E>) {
    vec.sort_unstable_by(|x, y| y.cmp(x));
}

pub fn eiter_mut<'a, C>(v: &'a mut Vert<C>) -> impl Iterator<Item = &'a mut C> {
    v.iter_mut().flatten()
}

pub fn eiter_mut_external<'a, C>(v: &'a mut Vert<C>) -> impl Iterator<Item = &'a mut Option<C>> {
    v.iter_mut()
}

pub fn eiter<'a, C>(v: &'a Vert<C>) -> impl Iterator<Item = &'a C> {
    v.iter().flatten()
}
