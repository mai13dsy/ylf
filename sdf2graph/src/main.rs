use std::error::Error;
use std::fs::{read_dir, File};
use std::io::BufRead;
use std::io::BufReader;
use std::io::Write;
use std::num::NonZeroU8;
use std::path::PathBuf;
use structopt::StructOpt;
use ptable::Element;

pub mod struts;

#[derive(StructOpt, Debug)]
/// Converts .sdf files to properitary .graph format
/// can only handle a single mol definition per file
struct Opt {
    /// directory
    input: PathBuf,
    /// directroy
    output: PathBuf,
    /// strip hydrogen from graph (Nodes labeled H)
    #[structopt(short, long)]
    strip_h: bool,
}

fn main() -> Result<(), Box<dyn Error>> {
    let opt = Opt::from_args();

    for entry in read_dir(opt.input)? {
        let mut entry = entry?.path();
        if !entry.is_file() {
            continue;
        };
        if entry.extension().and_then(std::ffi::OsStr::to_str) != Some("sdf") {
            println!("skipping {:?} due to wrong file extension", entry);
            continue;
        };
        println!("reading from {:?}", entry);
        let rdr = File::open(&entry)?;
        let bufrdr = BufReader::new(rdr);
        let mut graph = parse(bufrdr)?;

        if opt.strip_h {
            entry.set_extension("dot");
            let file = entry.file_name().unwrap();
            let mut out = opt.output.clone();
            out.push(file);
            let file = File::create(out)?;
            graph.dot(file);

            graph.filter(|n| *n == Element::Hydrogen);

            entry.set_extension("dot2");
            let file = entry.file_name().unwrap();
            let mut out = opt.output.clone();
            out.push(file);
            let file = File::create(out)?;
            graph.dot(file);
        }

        entry.set_extension("graph");
        let file = entry.file_name().unwrap();
        let mut out = opt.output.clone();
        out.push(file);
        println!("writing to {:?}", out);
        let file = File::create(out)?;
        write_graph(graph, file)?;
    }
    Ok(())
}
fn parse<R: BufRead>(r: R) -> Result<struts::MolGraph<Element, u8>, Box<dyn Error>> {
    let mut r = r.lines();
    let _title = r.next().unwrap()?;
    let _program = r.next().unwrap()?;
    let _comment = r.next().unwrap()?;

    let counts = r.next().unwrap()?;
    let mut counts = counts.split_whitespace();
    let atoms: usize = counts.next().unwrap().parse()?;
    let bonds: usize = counts.next().unwrap().parse()?;
    let _atlist: usize = counts.next().unwrap().parse()?;

    let nodes: Result<Vec<_>, _> = r
        .by_ref()
        .take(atoms)
        .map(|n| n.map(|n| n.split_whitespace().nth(3).unwrap().to_owned()))
        .map(|nr| nr.map(|n| Element::from_symbol(&n).unwrap()))
        .collect();
    let nodes = nodes?;

    let edges: Result<Vec<_>, Box<dyn Error>> = r
        .take(bonds)
        .map(|e| {
            let e = e?;
            let mut elements = e.split_whitespace();
            let from: u8 = elements.next().unwrap().parse()?;
            let to: u8 = elements.next().unwrap().parse()?;
            let bond: u8 = elements.next().unwrap().parse()?;

            let from = NonZeroU8::new(from).unwrap();
            let to = NonZeroU8::new(to).unwrap();
            Ok((from, to, bond))
        })
        .collect();
    let edges = edges?;

    let mut graph = struts::MolGraph::new_with_label(Element::Hydrogen);
    for node in nodes {
        graph.push_node(node);
    }

    for (from, to, label) in edges {
        graph.push_edge(from, to, label)
    }

    Ok(graph)
}

fn write_graph<W: Write>(
    graph: struts::MolGraph<Element, u8>,
    mut w: W,
) -> Result<(), Box<dyn Error>> {
    // apparently #-lines are comments anyway and produce problems sometimes
    //writeln!(w, "#author;nobody")?;
    let (nodes, edges) = graph.sizes();
    writeln!(w, "#nodes;{}", nodes)?;
    writeln!(w, "#edges;{}", edges)?;
    writeln!(w, "Nodes labelled;True")?;
    writeln!(w, "Edges labelled;True")?;
    writeln!(w, "Directed Graph;false")?;
    writeln!(w)?;

    for (idx, node) in graph.nodes() {
        writeln!(w, "{};{}", idx, Element::get_symbol(&node.0))?;
    }
    writeln!(w)?;

    for (idx, node) in graph.nodes() {
        for (to, label) in struts::eiter(&node.1) {
            writeln!(w, "{};{};{}", idx, to, label)?;
        }
    }
    Ok(())
}
